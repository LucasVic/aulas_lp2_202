<?php 
defined('BASEPATH') OR exit('no direct script access allowed');

class loginModel extends CI_Model{
    
    public function verifica(){
        if(count($_POST) == 0)
            return 0;
        
       /*  if(strcmp($_POST['email'], 'admin@admin.com') == 0)
            if(strcmp($_POST['senha'], '12345') == 0){
                redirect('home');
            } 
            return 1;
        */

        $email = $this->input->post('email');
        $senha = $this->input->post('senha');
        
        $this->load->library('Login', '', 'acesso'); // Necessário trocar o nome da classe pois 'login' já é um nome utilizado pelo próprio CodeIgniter
        $n = $this->acesso->verifica($email, $senha);

        if ($n)
            redirect('home');
        return 1;
    }

}

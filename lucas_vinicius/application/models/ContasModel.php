<?php 
defined('BASEPATH') OR exit('no direct script access allowed');

class ContasModel extends CI_Model{
    
    public function __construct(){
        $this->load->library("Conta", "", "bill");
    }

    public function cria($tipo){
        if(count($_POST) == 0)
            return;

        $data = $this->input->post();
        $this->bill->cria($data);
    }

    public function lista($tipo){
        $v = $this->bill->lista($tipo);
        return $v;
    }

}

<?php
defined('BASEPATH') or exit('no direct script access allowed');

class Contas extends MY_Controller{

    public function index(){

        $this->show("Lista de todas as contas");

    }

    // Ambos os métodos seguintes vão:
    // 1º Salvar os dados da nova conta no banco (caso existam)
    // 2º Recuperar do banco uma lista de todas as contas (a pagar e a receber)
    // 3º Carregar uma view mandando a lista de contas como argumento
    // 5º Exibir a mesma view em questão  

    public function pagar(){
        //1º:
        $this->load->model("ContasModel", "conta");
        $this->conta->cria("pagar");

        //2º:
        $v['lista'] = $this->conta->lista("pagar");
        $v['tipo'] = "pagar";

        //3º:
        $html = $this->load->view("contas/form_contas", $v, true);
        $html .= $this->load->view("contas/lista_contas", $v, true);

        //4º:
        $this->show($html);
    }

    public function receber(){

        //1º:
        $this->load->model("ContasModel", "conta");
        $this->conta->cria("receber");

        //2º:
        $v['lista'] = $this->conta->lista("receber");
        $v['tipo'] = "receber";

        //3º:
        $html = $this->load->view("contas/form_contas", $v, true);
        $html .= $this->load->view("contas/lista_contas", $v, true);

        //4º:
        $this->show($html);

    }

}
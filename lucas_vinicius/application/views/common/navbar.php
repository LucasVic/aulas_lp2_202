<nav class="navbar navbar-expand-lg navbar-dark primary-color">

    <a class="navbar-brand" href="#">Controle Financeiro</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#MenuDeNavegacao"
        aria-controls="MenuDeNavegacao" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="MenuDeNavegacao">

        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<?= base_url('home') ?>">Home
                    <span class="sr-only">(current)</span>
                </a>
            </li>
            
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="dropCadastro" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">Cadastro</a>
                <div class="dropdown-menu dropdown-primary" aria-labelledby="dropCadastro">
                    <a class="dropdown-item" href="<?= base_url('usuario/cadastro') ?>">Usuário</a>
                    <a class="dropdown-item" href="#">Conta Bancária</a>
                    <a class="dropdown-item" href="#">Parceiros</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="dropLancamentos" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">Lançamentos</a>
                <div class="dropdown-menu dropdown-primary" aria-labelledby="dropLancamentos">
                    <a class="dropdown-item" href="<?= base_url('contas/pagar') ?>">Contas a Pagar</a>
                    <a class="dropdown-item" href="<?= base_url('contas/receber') ?>">Contas a Receber</a>
                    <a class="dropdown-item" href="#">Fluxo de Caixa</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="dropRelatorios" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">Relatórios</a>
                <div class="dropdown-menu dropdown-primary" aria-labelledby="dropRelatorios">
                    <a class="dropdown-item" href="#">Lançamentos do Período</a>
                    <a class="dropdown-item" href="#">Resumo Mensal</a>
                    <a class="dropdown-item" href="#">Resumo Anual</a>
                </div>
            </li>

        </ul>

  </div>

</nav>
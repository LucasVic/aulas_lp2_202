<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto border mt-5 pt-3 pb-3">
            <form method="POST" id="contas_form">
            
                <input type="text" name="parceiro" class="form-control" placeholder="Devedor / Credor"><br>
                <input type="text" name="descricao" class="form-control" placeholder="Descrição"><br>
                <input type="number" name="mes" class="form-control" placeholder="Mês" min="1" max="12"><br>
                <label for="ano"> Ano: </label>
                <select name="ano" id="ano">
                    <option value="" disabled="true"> Escolha... </option>
                    <?php
                        for($i = 1995; $i <= 2020; $i++){
                            print("<option value='$i'> $i </option>");
                        }
                    ?>
                </select><br><br>
                <input type="number" name="valor" class="form-control" placeholder="Valor"><br><br>
                <input type="hidden" name="tipo" value="<?= $tipo ?>">

                <div>
                    <a class="btn btn-primary" onclick="document.getElementById('contas_form').submit();">Enviar</a>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col mx-auto border mt-5 pt-3 pb-3">
            <table class="table">
                <thead class="black white-text">
                    <tr class="blue white-text">
                        <th colspan="7" class="text-center"> Contas a <?= $tipo ?></th>
                    </tr>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Parceiro</th>
                        <th scope="col">Descrição</th>
                        <th scope="col">Valor</th>
                        <th scope="col">Mês</th>
                        <th scope="col">Ano</th>
                        <th scope="col">Data</th>
                    </tr>
                </thead>
                <tbody>
                        <?php
                            for($i = 0; $i < sizeof($lista); $i++){
                                echo("
                                    <tr>
                                        <th> ".$lista[$i]['id']." </th>
                                        <td> ".$lista[$i]['parceiro']." </td>
                                        <td> ".$lista[$i]['descricao']." </td>
                                        <td> ".$lista[$i]['valor']." </td>
                                        <td> ".$lista[$i]['mes']." </td>
                                        <td> ".$lista[$i]['ano']." </td>
                                        <td> ".$lista[$i]['created_at']." </td>
                                    </tr>
                                ");
                            }
                        ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

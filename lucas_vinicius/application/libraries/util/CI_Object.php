<?php

    class CI_Object{
        // Classe necessária para que nossas bibliotecas tenham conhecimento sobre os mecanismos de CodeIgniter
        public function __get($key){
            return get_instance()->$key;
        }

    }
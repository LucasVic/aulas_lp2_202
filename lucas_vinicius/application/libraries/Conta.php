<?php
    defined('BASEPATH') OR exit('no direct script access allowed');
    include_once APPPATH.'libraries/util/CI_Object.php'; // Incluindo a classe de conexão para herdá-la

    class Conta extends CI_Object{

        public function cria($data){
            $this->db->insert('conta', $data);
            return $this->db->insert_id();
        }

        public function lista($tipo){
            $data = ['tipo' => $tipo];
            $res = $this->db->get_where('conta', $data);
            return $res->result_array();
        }

    }